#include <uapi/linux/ptrace.h>
#include <net/sock.h>
#include <bcc/proto.h>

#define ETH_ALEN  6 // Octets in one ethernet addr
#define ETH_HLEN  14 // Total octets in header.
#define IS_PSEUDO 0x10
#define PORT_QUEUE_SIZE 1000

#define bpfoffsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define IP_CSUM_OFF (ETH_HLEN + bpfoffsetof(struct ip_t, hchecksum))
#define TCP_CSUM_OFF (ETH_HLEN + sizeof(struct ip_t) + bpfoffsetof(struct tcp_t, cksum))
#define TCP_SRC_PORT_OFF (ETH_HLEN + sizeof(struct ip_t) + bpfoffsetof(struct tcp_t, src_port))
#define TCP_DST_PORT_OFF (ETH_HLEN + sizeof(struct ip_t) + bpfoffsetof(struct tcp_t, dst_port))
#define IP_DST_OFF (ETH_HLEN + bpfoffsetof(struct ip_t, dst))
#define IP_SRC_OFF (ETH_HLEN + bpfoffsetof(struct ip_t, src))

// #define _htonl __builtin_bswap32

struct ip_port_t
{
  u32 ip;
  u16 port;
};

struct ip_port_fin_t
{
  u32 ip;
  u16 port;
  u8 fin;
};

BPF_TABLE("hash", u16, u32, tb_s_ips, 16);
BPF_TABLE("array", int, u32, tb_init_load_ip, 2);

BPF_TABLE("hash",struct ip_port_t, struct ip_port_t, tb_server, 1024);
BPF_TABLE("hash",struct ip_port_t, struct ip_port_fin_t, tb_client, 1024);
BPF_TABLE("hash",u32, u64, tb_mac, 1024);

// tb_rr_idx: 0: which server to choose, 1: total number of servers
BPF_TABLE("array", int, u16, tb_rr_idx, 2);
// This array will be use as a FIFO queue to reuse port.
// when we need a port, we pop one. when finish a session, the port is push into queue.
BPF_TABLE("array", int, u16, tb_port_queue, PORT_QUEUE_SIZE);
BPF_TABLE("array", int, int, tb_head_tail, 2);
// BPF_TABLE("array", int, _leaf_type, _name, _size)
// 




static inline void set_tcp_dst_port(struct __sk_buff *skb, u16 new_dst_port)
{
  u16 old_dst_port = htons(load_half(skb, TCP_DST_PORT_OFF));

  bpf_l4_csum_replace(skb, TCP_CSUM_OFF, old_dst_port, new_dst_port,sizeof(new_dst_port));
  bpf_skb_store_bytes(skb, TCP_DST_PORT_OFF, &new_dst_port, sizeof(new_dst_port), 0);
}

static inline void set_tcp_src_port(struct __sk_buff *skb, u16 new_src_port)
{
  u16 old_src_port = htons(load_half(skb, TCP_SRC_PORT_OFF));

  bpf_l4_csum_replace(skb, TCP_CSUM_OFF, old_src_port, new_src_port,sizeof(new_src_port));
  bpf_skb_store_bytes(skb, TCP_SRC_PORT_OFF, &new_src_port, sizeof(new_src_port), 0);
}

static inline void set_tcp_ip_dst(struct __sk_buff *skb, u32 new_ip)
{
  // u32 old_ip = _htonl(load_word(skb, IP_DST_OFF));
  u32 old_ip = htonl(load_word(skb, IP_DST_OFF));

  bpf_l4_csum_replace(skb, TCP_CSUM_OFF, old_ip, new_ip, IS_PSEUDO | sizeof(new_ip));
  bpf_l3_csum_replace(skb, IP_CSUM_OFF, old_ip, new_ip, sizeof(new_ip));
  bpf_skb_store_bytes(skb, IP_DST_OFF, &new_ip, sizeof(new_ip), 0);
}

static inline void set_tcp_ip_src(struct __sk_buff *skb, u32 new_ip)
{
  // u32 old_ip = _htonl(load_word(skb, IP_SRC_OFF));
  u32 old_ip = htonl(load_word(skb, IP_SRC_OFF));

  bpf_l4_csum_replace(skb, TCP_CSUM_OFF, old_ip, new_ip, IS_PSEUDO | sizeof(new_ip));
  bpf_l3_csum_replace(skb, IP_CSUM_OFF, old_ip, new_ip, sizeof(new_ip));
  bpf_skb_store_bytes(skb, IP_SRC_OFF, &new_ip, sizeof(new_ip), 0);
}

int hello(struct __sk_buff *skb) {

  u8 *cursor = 0;
  u16 identity_port = 8080; // dst port when send to S
  // parsing packet structure
  struct ethernet_t *ethernet = cursor_advance(cursor, sizeof(*ethernet));
  if (ethernet->type != 0x0800) { // IP
    goto DROP;  
  }

  struct ip_t *ip = cursor_advance(cursor, sizeof(*ip));
  if (ip->nextp !=  0x06) { // TCP
    goto DROP;
  }


  struct tcp_t *tcp = cursor_advance(cursor, sizeof(*tcp));

  if (!(tcp->dst_port == identity_port || tcp->src_port == identity_port)) {
    goto DROP;
  }


  u32 new_src_ip, new_dst_ip;//, *tmp_ip;
  u64 *tmp_mac;

  // for implementing round-robin
  int k_rr_idx = 0;
  u16 zero = 0, *rr_cnt;
  rr_cnt = tb_rr_idx.lookup_or_init(&k_rr_idx, &zero);

  int k_num_servers = 1;
  u16 num_servers = 1, *num_servers_p;
  num_servers_p = tb_rr_idx.lookup(&k_num_servers);
  if (num_servers_p) {
    num_servers = *num_servers_p;
  }

  u16 tmp_port;
  struct ip_port_t client = {};
  struct ip_port_t server = {};
  struct ip_port_fin_t server_ffin = {};

  int head_idx = 0, tail_idx = 1;
  int idx_0 = 0, idx_1 = 1;
  int idx_s_0 = 0, idx_s_1=1;

  /*
  TOPOLOGY:
  Client (C) ------------- Load Balance (L) ----------            -------- Webserver (S)
                        Bridge--(VM)--Host only      |            |    Host only--(VM)    
                                                    VSwitch (host-only)
                                                                  |
                                                                  -------- WebServer(S2)
  */

  u32 C_ip = 0, LC_ip = 0, LS_ip = 0, S_ip = 0;
  u64 C_mac = 0, LC_mac = 0, LS_mac = 0, S_mac = 0;
  u16 to_S_port = 0, to_C_port = 0;

  u32 serv_ip1=0, serv_ip2=0;
  u64 serv_mac1=0, serv_mac2=0;



  // Setup ip address for load balancer
  u32 *LC_ip_p =tb_init_load_ip.lookup(&idx_0);
  if (LC_ip_p){
      LC_ip = *LC_ip_p;//2380223248; //141.223.83.16  //2380223254; // 141.223.83.22
  }
  u32 *LS_ip_p =tb_init_load_ip.lookup(&idx_1);
  if (LS_ip_p){
      LS_ip = *LS_ip_p;//3232249958;  //192.168.56.102  //3232249957;// 192.168.56.101
  }

  // Setup mac address for load balancer
  u64 *LC_mac_p = tb_mac.lookup(&LC_ip);
  if (LC_mac_p) {
    LC_mac = *LC_mac_p;
  }

  u64 *LS_mac_p = tb_mac.lookup(&LS_ip);
  if (LS_mac_p) {
    LS_mac = *LS_mac_p;
  }




  //-------------------------------------------------
  // update IP-MAC
  u32 k_ip;
  u64 mac_val;
  if (tcp->dst_port == identity_port) {
    k_ip = ip->src;
    if (tb_mac.lookup(&k_ip) == 0) {
      mac_val = ethernet->src;
      tb_mac.update(&k_ip, &mac_val);
    }
  }

  //-------------------------------------------------
  // update mapping (ip,port) between server and client. This ensure that we can differentiate connection
  if (tcp->dst_port == identity_port && ip->dst == LC_ip) {
    
    client.ip = ip->src;
    client.port = tcp->src_port;

    // if no entry, make one for server by doing Round Robin
    if (tb_client.lookup(&client) == 0) {
      u16 rr_idx;
      rr_idx = *rr_cnt;
      u32 *tmp_ip = tb_s_ips.lookup(&(rr_idx));
      if (tmp_ip) 
        server.ip = *tmp_ip;
      // Update rr_cnt
      if ((*rr_cnt) >= num_servers - 1) {
        (*rr_cnt) = 0;
      }
      else {
        (*rr_cnt) ++;
      }

      // Assign port by poping it from FIFO
      int *head_p = tb_head_tail.lookup(&head_idx);
      if (head_p) {
        int port_idx = *head_p;
        u16 *port_p = tb_port_queue.lookup(&port_idx);
        if (port_p) {
          server.port = *port_p;
          // next head_p
          if ((*head_p) >= PORT_QUEUE_SIZE - 1) {
            (*head_p)=0;
          }
          else {
            (*head_p)++;
          }
        }
      }

      server_ffin.ip = server.ip;
      server_ffin.port = server.port;
      server_ffin.fin = 0;
      tb_client.update(&client, &server_ffin);
      // Also update for tb_server
      tb_server.update(&server, &client);
    }
    // -----------------------------------------------
    // FINISH flag, then update fin field
    if (tcp->flag_fin) {
      client.ip = ip->src;
      client.port = tcp->src_port;

      struct ip_port_fin_t *ip_port_fin_p;
      ip_port_fin_p = tb_client.lookup(&client);
      if (ip_port_fin_p) {
        server_ffin = *ip_port_fin_p;
        server_ffin.fin = 1;
        tb_client.delete(&client);
        tb_client.update(&client, &server_ffin);
      }
    }
  }


  // ----------------------TO SERVER------------------
  if (tcp->dst_port == identity_port && ip->dst == LC_ip) {

    // get server ip and port
    struct ip_port_fin_t *ip_port_fin_p;
    client.ip = ip->src;
    client.port = tcp->src_port;
    ip_port_fin_p = tb_client.lookup(&client);
    
    if (ip_port_fin_p) {
      server_ffin = *ip_port_fin_p;
      S_ip = server_ffin.ip;
      to_S_port = server_ffin.port;
    }

    u64 *S_mac_p = tb_mac.lookup(&S_ip);
    if (S_mac_p) {
      S_mac = *S_mac_p;
    }

    // Override mac address
    ethernet->dst = S_mac;
    ethernet->src = LS_mac;

    set_tcp_ip_dst(skb, htonl(S_ip));
    set_tcp_ip_src(skb, htonl(LS_ip));
    set_tcp_src_port(skb, ntohs(to_S_port));

    // 3 is idx of LS eth
    // 0: redirect to Egress (if 1: inress)
    return bpf_redirect(3, 0); 
    // bpf_clone_redirect(skb, 3, 0); 
    // return 1;
  } 


  // ----------------------FROM SERVER------------------
  if (ip->dst == LS_ip && tcp->src_port == identity_port) {

    // get back client ip, port to send
    struct ip_port_t *ip_port_p;
    server.ip = ip->src;
    server.port = tcp->dst_port;
    ip_port_p = tb_server.lookup(&server);
    if (ip_port_p) {
      client = *ip_port_p;
      // (*total_ip) = client.ip; //test
      C_ip = client.ip;
      to_C_port = client.port;

      // if finish session, then delete all mapping
      struct ip_port_fin_t *ip_port_fin_p;
      ip_port_fin_p = tb_client.lookup(&client);
      if (ip_port_fin_p) {
        server_ffin = *ip_port_fin_p;
        if (server_ffin.fin) {
          tb_client.delete(&client);
          server.ip = server_ffin.ip;
          server.port = server_ffin.port;
          tb_server.delete(&server);
          
          // reuse port by push it into fifo
          int *tail_p = tb_head_tail.lookup(&tail_idx);
          if (tail_p) {
            int port_idx = *tail_p;
            u16 *port_p = tb_port_queue.lookup(&port_idx);
            if (port_p) {
              *port_p = server.port;
              // next tail_p
              if ((*tail_p) >= PORT_QUEUE_SIZE - 1) {
                (*tail_p)=0;
              }
              else {
                (*tail_p)++;
              }
            }
          }
        }
      }
    }

    // get mac ADDR
    u64 *mac_val_p;
    mac_val_p  = tb_mac.lookup(&C_ip);
    if (mac_val_p) {
      C_mac = *mac_val_p;
    }

    // Override mac address
    ethernet->dst = C_mac;
    ethernet->src = LC_mac;

    set_tcp_ip_dst(skb, htonl(C_ip));
    set_tcp_ip_src(skb, htonl(LC_ip));
    set_tcp_dst_port(skb, ntohs(to_C_port));

    // 2 is index of LC eth
    // 0: redirect to Egress (if 1: egress)
    return bpf_redirect( 2, 0); 
    // bpf_clone_redirect(skb, 2, 0); 
    // return 1;
  } 

  DROP:

  // return value: 0: bypass, 1: do action in the filter in load_balancer.py
  return 0;
}