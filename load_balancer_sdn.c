#include <uapi/linux/ptrace.h>
#include <net/sock.h>
#include <bcc/proto.h>

#define ETH_ALEN  6 // Octets in one ethernet addr
#define ETH_HLEN  14 // Total octets in header.
#define IS_PSEUDO 0x10

#define bpfoffsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define IP_CSUM_OFF (ETH_HLEN + bpfoffsetof(struct ip_t, hchecksum))
#define TCP_CSUM_OFF (ETH_HLEN + sizeof(struct ip_t) + bpfoffsetof(struct tcp_t, cksum))
#define IP_DST_OFF (ETH_HLEN + bpfoffsetof(struct ip_t, dst))
#define IP_SRC_OFF (ETH_HLEN + bpfoffsetof(struct ip_t, src))


struct to_server_key_t {
  u32 src_ip;
  u16 src_port;
  u16 dst_port;
};

struct to_server_val_t {
  u64 src_mac;
  u64 dst_mac;
  u32 dst_ip;
};
struct to_client_key_t {
  u32 dst_ip;
  // u16 src_port;
};

struct to_client_val_t {
  u64 src_mac;
  u64 dst_mac;
  u32 src_ip;
};

BPF_TABLE("hash", struct to_server_key_t, struct to_server_val_t, tb_to_server, 10240);
BPF_TABLE("hash", struct to_client_key_t, struct to_client_val_t, tb_to_client, 10240);

static inline void set_tcp_ip_dst(struct __sk_buff *skb, u32 new_ip) {
  u32 old_ip = htonl(load_word(skb, IP_DST_OFF));

  bpf_l4_csum_replace(skb, TCP_CSUM_OFF, old_ip, new_ip, IS_PSEUDO | sizeof(new_ip));
  bpf_l3_csum_replace(skb, IP_CSUM_OFF, old_ip, new_ip, sizeof(new_ip));
  bpf_skb_store_bytes(skb, IP_DST_OFF, &new_ip, sizeof(new_ip), 0);
}

static inline void set_tcp_ip_src(struct __sk_buff *skb, u32 new_ip) {
  u32 old_ip = htonl(load_word(skb, IP_SRC_OFF));

  bpf_l4_csum_replace(skb, TCP_CSUM_OFF, old_ip, new_ip, IS_PSEUDO | sizeof(new_ip));
  bpf_l3_csum_replace(skb, IP_CSUM_OFF, old_ip, new_ip, sizeof(new_ip));
  bpf_skb_store_bytes(skb, IP_SRC_OFF, &new_ip, sizeof(new_ip), 0);
}

int hello(struct __sk_buff *skb) {

  u8 *cursor = 0;
  // parsing packet structure
  struct ethernet_t *ethernet = cursor_advance(cursor, sizeof(*ethernet));
  if (ethernet->type != 0x0800) { // IP
    goto PASS;  
  }
  struct ip_t *ip = cursor_advance(cursor, sizeof(*ip));
  if (ip->nextp !=  0x06) { // TCP
    goto PASS;
  }
  struct tcp_t *tcp = cursor_advance(cursor, sizeof(*tcp));


  /*
  TOPOLOGY:
  Client (C) ------------- Load Balance (L) ----------            -------- Webserver (S)
                        Bridge--(VM)--Host only      |            |    Host only--(VM)    
                                                    VSwitch (host-only)
                                                                  |
                                                                  -------- WebServer(S2)
  */

  // ----------------------TO SERVER------------------
  // struct to_server_key_t to_server_key = {
  //   .src_ip = ip->src,
  //   .src_port = tcp->src_port,
  //   .dst_port = tcp->dst_port
  // };

  // bpf_trace_printk("Hello!\n");

  struct to_server_key_t to_server_key = {};
  to_server_key.src_ip = ip->src;
  to_server_key.src_port = tcp->src_port;
  to_server_key.dst_port = tcp->dst_port;


  struct to_server_val_t *to_server_val_p = tb_to_server.lookup(&to_server_key);
  if (to_server_val_p) {
    ethernet->dst = to_server_val_p->dst_mac;
    ethernet->src = to_server_val_p->src_mac;
    set_tcp_ip_dst(skb, htonl(to_server_val_p->dst_ip));

     // if finish flag
    if (tcp->flag_fin) {
      tb_to_server.delete(&to_server_key);
    }

    return bpf_redirect(3, 0); 
  }

  if (tcp->dst_port == 8080) {
    // FIXME: fix hard-coded interface here
    return bpf_redirect(4, 1);
  }

  // ----------------------TO CLIENT------------------
  // struct to_client_key_t to_client_key = {
  //   .dst_ip = ip->dst,
  //   .src_port = tcp->src_port
  // };
  
  struct to_client_key_t to_client_key = {};
  to_client_key.dst_ip = ip->dst;
  // to_client_key.src_port = tcp->src_port;

  struct to_client_val_t *to_client_val_p = tb_to_client.lookup(&to_client_key);
  if (to_client_val_p) {
    ethernet->dst = to_client_val_p->dst_mac;
    ethernet->src = to_client_val_p->src_mac;
    set_tcp_ip_src(skb, htonl(to_client_val_p->src_ip));

    return bpf_redirect( 2, 0); 
  }

  PASS:

  // return value: 0: bypass, 1: do action in the filter in load_balancer.py
  return 0;
}

int recvfrom_controller(struct __sk_buff *skb) {

  bpf_trace_printk("Hello 1!\n");
  
  u8 *cursor = 0;
  // parsing packet structure
  struct ethernet_t *ethernet = cursor_advance(cursor, sizeof(*ethernet));
  if (ethernet->type != 0x0800) { // IP
    goto PASS;  
  }
  struct ip_t *ip = cursor_advance(cursor, sizeof(*ip));
  if (ip->nextp !=  0x06) { // TCP
    goto PASS;
  }
  struct tcp_t *tcp = cursor_advance(cursor, sizeof(*tcp));

  struct to_server_key_t to_server_key = {};
  to_server_key.src_ip = ip->src;
  to_server_key.src_port = tcp->src_port;
  to_server_key.dst_port = tcp->dst_port;


  struct to_server_val_t *to_server_val_p = tb_to_server.lookup(&to_server_key);
  if (to_server_val_p) {
    ethernet->dst = to_server_val_p->dst_mac;
    ethernet->src = to_server_val_p->src_mac;
    set_tcp_ip_dst(skb, htonl(to_server_val_p->dst_ip));
    return bpf_redirect(3, 0); 
  }

  PASS:

  // return value: 0: bypass, 1: do action in the filter in load_balancer.py
  return 0;
}