#!/usr/bin/env python

from bcc import BPF
from pyroute2 import IPRoute
from time import sleep
from netaddr import IPAddress

# Get ip address for load_balancer
import os
import socket
import fcntl
import struct

# Input ip address for servers
import sys 

# Get mac address for servers
import re
from subprocess import Popen, PIPE
import subprocess
import netifaces as nif 

# Execute OS command
def subprocess_open(command):
    popen = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (stdoutdata, stderrdata) = popen.communicate()
    return stdoutdata, stderrdata

# Get MAC address for load_balancer using ip address
def mac_for_ip(ip):
    'Returns a list of MACs for interfaces that have given IP, returns None if not found'
    for i in nif.interfaces():
        addrs = nif.ifaddresses(i)
        try:
            if_mac = addrs[nif.AF_LINK][0]['addr']
            if_ip = addrs[nif.AF_INET][0]['addr']
        except IndexError, KeyError: #ignore ifaces that don't have MAC or IP
            if_mac = if_ip = None
        if if_ip == ip:
            return if_mac
    return None

# Transfer type of ip to int
def ipaddrToint(IPaddr):
    IP_arr = IPaddr.split('.')
    # print IP_arr
    result = 0
    for i in range(0, 4):
        result += int(IP_arr[i])*pow(256, (3-i))
    return result;

# Transfer type of mac to int
def macaddrToint(MACaddr):
    MAC_arr = MACaddr.split(':')
    # print MAC_arr
    tmp =""
    for i in range(0, 6):
        tmp += MAC_arr[i]
    return int(tmp,16)
    # return tmp

def setTbVal(tb, key, val):
    k = tb.Key(key)
    leaf = tb.Leaf(val)
    tb[k] = leaf

# Input ip address for servers
if len(sys.argv) < 2:
    print("USAGE: load_balancer.py [server1_ip] [...] [server_n_ip]")
    exit()

numServers = len(sys.argv) - 1
server_ips = []
server_ip_macs = {}
for i in range(1, numServers + 1):
    server_ips.append(sys.argv[i])

# Get MAC address for servers
for i in range(1, numServers + 1):
    server = server_ips[i-1]
    subprocess_open("ping -c1 "+server)
    pid = Popen(["arp", "-n", server], stdout=PIPE)
    s = pid.communicate()[0]
    mac = re.search(r"(([a-f\d]{1,2}\:){5}[a-f\d]{1,2})", s).groups()[0]
    server_ip_macs[ipaddrToint(server)] = macaddrToint(mac)


# Get ip address for load_balancer
if os.name != "nt":
    def get_interface_ip(ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])

    def get_lan_ip(find_interface):
        ip = socket.gethostbyname(socket.gethostname())
        if ip.startswith("127.") and os.name != "nt":
            interfaces = [
                find_interface,
                # "enp0s3",
                # "enp0s8",
                ]
            for ifname in interfaces:
                try:
                    ip = get_interface_ip(ifname)
                    break
                except IOError:
                    pass
        return ip

    CL_ip = get_lan_ip("enp0s3")
    LS_ip = get_lan_ip("enp0s8")

# Get MAC address for load_balancer
CL_mac = mac_for_ip(CL_ip)
LS_mac = mac_for_ip(LS_ip)

CL_hex = macaddrToint(CL_mac)
LS_hex = macaddrToint(LS_mac)

CL_int = ipaddrToint(CL_ip)
LS_int = ipaddrToint(LS_ip)


ipr = IPRoute()
#####  sleep until Ctrl-C
try:
    # b = BPF(src_file="load_balancer.c", debug=0)
    b = BPF(src_file="load_balancer_dnat.c", debug=0)
    fn = b.load_func("hello", BPF.SCHED_CLS)

    # Set initial IP address for Load balancer
    tb_init_load_ip = b.get_table("tb_init_load_ip")
    setTbVal(tb_init_load_ip, 0, CL_int)
    setTbVal(tb_init_load_ip, 1, LS_int)

    # Set number of servers
    tb_rr_idx = b.get_table("tb_rr_idx")
    setTbVal(tb_rr_idx, 1, numServers)

    # Set inital IP address for two servers
    tb_s_ips = b.get_table("tb_s_ips")
    for idx, server_ip in enumerate(server_ip_macs.keys()):
        setTbVal(tb_s_ips, idx, server_ip)

    # Set inital mac address for two servers and the Load balancer
    tb_mac = b.get_table("tb_mac")
    for server_ip in server_ip_macs.keys():
        server_mac = server_ip_macs[server_ip]
        setTbVal(tb_mac, server_ip, server_mac)
    
    setTbVal(tb_mac, CL_int, CL_hex)
    setTbVal(tb_mac, LS_int, LS_hex)

    queue_size = 1000
    start_port = 40001
    tb_port_queue = b.get_table("tb_port_queue")
    for i in range(0, queue_size):
        setTbVal(tb_port_queue, i, start_port + i)

    tb_head_tail = b.get_table("tb_head_tail")
    setTbVal(tb_head_tail, 0, 0)
    setTbVal(tb_head_tail, 1, 0)

    ##### lookup the link in the "lo" interface, because in demo we send request in the same host PC
    
    # USE this function to lookup index of network interface 

    idx0 = ipr.link_lookup(ifname="enp0s3")[0]
    print idx0
    # ipr.tc("add", "ingress", idx0, "ffff:")
    # ipr.tc("add-filter", "bpf", idx0, ":1", fd=fn.fd,
    #     name=fn.name, parent="ffff:", action="drop", classid=1)

    ipr.tc("add", "clsact", idx0)
    ipr.tc("add-filter", "bpf", idx0, ":1", fd=fn.fd, name=fn.name,
          parent="ffff:fff2", classid=1, direct_action=True)
   
    idx1 = ipr.link_lookup(ifname="enp0s8")[0]
    print idx1
    # ipr.tc("add", "ingress", idx1, "ffff:")
    # ipr.tc("add-filter", "bpf", idx1, ":1", fd=fn.fd,
    #     name=fn.name, parent="ffff:", action="drop", classid=1)
    
    ipr.tc("add", "clsact", idx1)
    ipr.tc("add-filter", "bpf", idx1, ":1", fd=fn.fd, name=fn.name,
          parent="ffff:fff2", classid=1, direct_action=True)

    print("Press Ctrl-C to terminate...")
    # go sleep
    while True:
        sleep(1000)
except KeyboardInterrupt:
    pass

finally:
    # if "idx" in locals(): ipr.link_remove(idx)

    # tb = b.get_table("test_tb")
    # for k, v in sorted(tb.items(), key=lambda item: item[0].value):
    #     print k.value, v.value

    # tb_profile = b.get_table("tb_profile")
    # print tb_profile.items()

    # ipr.tc("del", "ingress", idx0, "ffff:")
    # ipr.tc("del", "ingress", idx1, "ffff:")
    ipr.tc("del", "clsact", idx1)
    ipr.tc("del", "clsact", idx0)

    print "--------exit--------"