#!/usr/bin/env python

from bcc import BPF
from pyroute2 import IPRoute
import time
import fractions

import os
import socket
import fcntl
import struct
import sys 

import re
from subprocess import Popen, PIPE
import subprocess
import netifaces as nif 
import binascii

import threading
from pysnmp.hlapi import *

# global variables
s_rr_seq = []
new_s_rr_seq = []
num_s_rr_seq = 0
new_num_s_rr_seq = 0
exit_flag = 0
lock = threading.Lock()

# @profile
def main():
    global s_rr_seq
    global new_s_rr_seq
    global num_s_rr_seq
    global new_num_s_rr_seq
    global exit_flag

    ETH_P_IP = 0x800
    PORT = 8080
    BUF_SIZE = 1600
    POLL_PERIOD = 10 # seconds
    
    ifaceC = "enp0s3"
    ifaceS = "enp0s8"
    ifaceT = "tap0"

    # Execute OS command
    def subprocess_open(command):
        popen = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (stdoutdata, stderrdata) = popen.communicate()
        return stdoutdata, stderrdata

    # Get MAC address for load_balancer using ip address
    def mac_for_ip(ip):
        'Returns a list of MACs for interfaces that have given IP, returns None if not found'
        for i in nif.interfaces():
            addrs = nif.ifaddresses(i)
            try:
                if_mac = addrs[nif.AF_LINK][0]['addr']
                if_ip = addrs[nif.AF_INET][0]['addr']
            except IndexError, KeyError: #ignore ifaces that don't have MAC or IP
                if_mac = if_ip = None
            if if_ip == ip:
                return if_mac
        return None

    # Transfer type of ip to int
    def ip_to_int(IPaddr):
        IP_arr = IPaddr.split('.')
        # print IP_arr
        result = 0
        for i in range(0, 4):
            result += int(IP_arr[i])*pow(256, (3-i))
        return result;

    # Transfer type of mac to int
    def mac_to_int(MACaddr):
        MAC_arr = MACaddr.split(':')
        # print MAC_arr
        tmp =""
        for i in range(0, 6):
            tmp += MAC_arr[i]
        return int(tmp,16)
        # return tmp

    def set_tb_val(tb, key, val):
        k = tb.Key(key)
        leaf = tb.Leaf(val)
        tb[k] = leaf

    # Input ip address for servers
    if len(sys.argv) < 2:
        print("USAGE: load_balancer.py [server1_ip] [...] [server_n_ip]")
        exit()

    numServers = len(sys.argv) - 1
    s_ips_str = []
    ip_macs = {}

    for i in range(1, numServers + 1):
        s_ips_str.append(sys.argv[i])

    # Get MAC address for servers
    for i in range(1, numServers + 1):
        server = s_ips_str[i-1]
        subprocess_open("ping -c1 "+server)
        pid = Popen(["arp", "-n", server], stdout=PIPE)
        s = pid.communicate()[0]
        mac = re.search(r"(([a-f\d]{1,2}\:){5}[a-f\d]{1,2})", s).groups()[0]
        ip_macs[ip_to_int(server)] = mac_to_int(mac)

    # server variables
    s_ips = [ip_to_int(s_ip_str) for s_ip_str in s_ips_str]

    # Get ip address for load_balancer
    if os.name != "nt":
        def get_interface_ip(ifname):
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])

        def get_lan_ip(find_interface):
            ip = socket.gethostbyname(socket.gethostname())
            if ip.startswith("127.") and os.name != "nt":
                interfaces = [
                    find_interface,
                    # "enp0s3",
                    # "enp0s8",
                    ]
                for ifname in interfaces:
                    try:
                        ip = get_interface_ip(ifname)
                        break
                    except IOError:
                        pass
            return ip

        CL_ip = get_lan_ip(ifaceC)
        LS_ip = get_lan_ip(ifaceS)

    # Get MAC address for load_balancer
    CL_mac = mac_for_ip(CL_ip)
    LS_mac = mac_for_ip(LS_ip)

    CL_hex = mac_to_int(CL_mac)
    LS_hex = mac_to_int(LS_mac)

    ipr = IPRoute()

    # -------------------------------------------------------------------------------
    # SNMP: get server load
    def get_servers_load(s_ips_str, numServers):
        s_loads = [0 for i in range(0, numServers)]
        for i in range(0, numServers):
            errorIndication, errorStatus, errorIndex, varBinds = next(
                getCmd(SnmpEngine(),
                       CommunityData('public', mpModel=0),
                       UdpTransportTarget((s_ips_str[i], 161)),
                       ContextData(),
                       ObjectType(ObjectIdentity('.1.3.6.1.4.1.2021.10.1.3.1')))
            )

            if errorIndication:
                print(errorIndication)
            elif errorStatus:
                print('%s at %s' % (errorStatus.prettyPrint(),
                                    errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
            else:
                oid, val = varBinds[0]
                s_loads[i] = 10 - min(10, int(round(float(val))))
                # print s_ips_str[i], " : ", s_loads[i]
        return s_loads
    
    # get the rr sequense
    def cal_rr_seq(s_loads, s_ips, numServers):
        gcd = reduce(fractions.gcd, s_loads)
        # print "gcd: ", gcd
        # generate scheduling sequense for rr
        s_rr_seq = []
        for i in range(0, numServers):
            num_of_sched = int(s_loads[i]/gcd)
            s_rr_seq.extend([s_ips[i] for _ in range(0, num_of_sched)])
        # print "scheduling sequense: ", s_rr_seq
        return s_rr_seq


    def cal_s_rr_seq (period):
        global new_s_rr_seq
        global new_num_s_rr_seq
        global exit_flag
        
        while not exit_flag:
            s_loads = get_servers_load(s_ips_str, numServers)
            tmp_s_rr_seq = cal_rr_seq(s_loads, s_ips, numServers)
            tmp_num_s_rr = len(new_s_rr_seq)
            
            lock.acquire()
            new_s_rr_seq = tmp_s_rr_seq
            new_num_s_rr_seq = tmp_num_s_rr
            lock.release()
            # print "new_s_rr_seq: ", new_s_rr_seq
            time.sleep(period)
        
    try:        
        #  setup the socket
        sockT = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_IP))
        sockT.bind((ifaceT, ETH_P_IP))

        b = BPF(src_file="load_balancer_sdn.c", debug=0)
        fn = b.load_func("hello", BPF.SCHED_CLS)
        fn2 = b.load_func("recvfrom_controller", BPF.SCHED_CLS)

        # ipr.link("add", ifname="l0", kind="veth", peer="l1")
        # ipr.link("add", ifname="tap0", kind="tuntap", mode="tap")
        
        idxC = ipr.link_lookup(ifname=ifaceC)[0]
        ipr.tc("add", "clsact", idxC)
        ipr.tc("add-filter", "bpf", idxC, ":1", fd=fn.fd, name=fn.name,
              parent="ffff:fff2", classid=1, direct_action=True)
       
        idxS = ipr.link_lookup(ifname=ifaceS)[0] 
        ipr.tc("add", "clsact", idxS)
        ipr.tc("add-filter", "bpf", idxS, ":1", fd=fn.fd, name=fn.name,
              parent="ffff:fff2", classid=1, direct_action=True)

        idxT = ipr.link_lookup(ifname=ifaceT)[0]
        print idxT
        ipr.tc("add", "clsact", idxT)
        ipr.tc("add-filter", "bpf", idxT, ":1", fd=fn2.fd, name=fn2.name,
              parent="ffff:fff3", classid=1, direct_action=True)


        print("Press Ctrl-C to terminate...")
        #----------

        tb_to_server = b.get_table("tb_to_server")
        tb_to_client = b.get_table("tb_to_client")

        snmp_thread = threading.Thread( target=cal_s_rr_seq, args=(POLL_PERIOD, ) )
        snmp_thread.start()
        
        # init values for chosing server
        s_idx = 0
        s_ip  = s_ips[s_idx]

        pkt = sockT.recv(BUF_SIZE)
        
        while len(pkt) > 0:
            if(len(pkt)) > 54:
                # pkt = pkt[0]
                ethHeader = pkt[0:14]
                ipHeader  = pkt[14:34]
                
                ethH = struct.unpack("!6s6s2s",ethHeader)
                ipH  = struct.unpack('!BBHHHBBH4s4s' , ipHeader)
                if (ipH[6] != 6): # TCP
                    pkt = sockT.recv(BUF_SIZE)
                    continue
                
                tcpHeader = pkt[34:54]
                tcpH = struct.unpack('!HHLLBBHHH' , tcpHeader)
                if (tcpH[1] != PORT): 
                    pkt = sockT.recv(BUF_SIZE)
                    continue

                smac  = long(binascii.hexlify(ethH[1]), 16)
                saddr = int(binascii.hexlify(ipH[8]), 16)
                daddr = int(binascii.hexlify(ipH[9]), 16)
                
                # add entry to table
                key = tb_to_server.Key(saddr, tcpH[0], PORT)
                val = tb_to_server.Leaf(LS_hex, ip_macs[s_ip], s_ip)
                tb_to_server[key] = val
                
                key = tb_to_client.Key(saddr)
                val = tb_to_client.Leaf(CL_hex, smac, daddr)
                tb_to_client[key] = val
                
                # send back the packet to the kernel through tap interface
                sockT.send(pkt)
            
                # update next server
                if (s_idx < num_s_rr_seq-1):
                    s_idx += 1
                else:
                    s_idx = 0
                    # start new server sequense
                    lock.acquire()
                    s_rr_seq = new_s_rr_seq
                    num_s_rr_seq = new_num_s_rr_seq
                    lock.release()
                s_ip = s_rr_seq[s_idx]
                
            #  continue receving pkt
            pkt = sockT.recv(BUF_SIZE)

    except KeyboardInterrupt:
        pass

    finally:
        for idx in [idxS, idxC, idxT]:
            ipr.tc("del", "clsact", idx)

        print "final s_rr_seq: ", s_rr_seq
        
        exit_flag = 1
        snmp_thread.join()
        
        print "--------exit--------"

if __name__ == "__main__":
    main()